package com.steeplesoft.kotlin.kotlinrs

data class Book(var name : String, var description : String) {
    constructor() : this("", "")
}