package com.steeplesoft.kotlin.kotlinrs

import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application

import java.util.HashSet

@ApplicationPath("resources")
class MyApplication : Application() {
    override fun getClasses(): MutableSet<Class<*>>? {
        val classes = HashSet<Class<*>>()
        classes.add(BookResource::class.java)
        return classes
    }
}