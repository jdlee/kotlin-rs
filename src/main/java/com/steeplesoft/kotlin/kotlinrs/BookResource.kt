package com.steeplesoft.kotlin.kotlinrs

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam

@Path("/books")
class BookResource {

    @GET
    fun getBooks(): Array<Book> {
        return arrayOf(
                Book("Book 1", "Book 1"),
                Book("Book 2", "Book 2"),
                Book("Book 3", "Book 3"))
    }

    @GET
    @Path("{id}")
    fun getBook(@PathParam("id") id: String): Book {
        return Book("Book " + id, "Description " + id)
    }
}